# Smart Contract Auctions
Smart contract based implementation of several different auction types.

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Setup](#setup)
* [Usage](#usage)
* [Features](#features)


## General Information
This repository contains the code of my software project in the course "Distributed Systems" at FU Berlin. The task was to get to know and create smart contract based auctions on Ethereum.

The focus in all auctions lies on understandablity. Therefor, the contracts are not gas price optimized and **not production ready**. Moreover, I skipped selling actual items like NFTs. The necessary changes would not be very complex but I focused only on the raw auctions.


## Technologies Used
- [Solidity](https://docs.soliditylang.org/en/v0.8.15/) for writing the smart contracts
- [Brownie](https://eth-brownie.readthedocs.io/en/stable/) a Python-based development and testing framework for smart contracts


## Setup

To install brownie follow the official [Brownie documentation](https://eth-brownie.readthedocs.io/en/stable/install.html).

I would recommend the second method described there. Just create a venv and then use `pip install eth-brownie` instead of the recommended method. It's easier to do than the pipx method and keeps everything in the virtual environment.

The steps are the following:

- Navigate to the root of the project directory 
- To create a virtual environment run `python3 -m venv venv`
- To activate it run `source venv/bin/activate` (MacOS and Linux only probably)
- To install brownie run `pip install eth-brownie`

The `brownie` keyword is only available inside the virtual environment now - don't forget to activate it! It's activated when to see "(venv)" leading in the terminal.

## Usage

To run deploy or test scripts use the following brownie commands.
Whenever a script is executed brownie starts a local [ganache-cli](https://docs.nethereum.com/en/latest/ethereum-and-clients/ganache-cli/) blockchain. The default network can be specified in the _brownie-config.yaml_. 

Run scripts from _scripts_ directory:

`brownie run _script_name_`

Run all test scripts from _tests_ directory:

`brownie test -s` 

Run only specified test script:

`brownie test tests/_script_name_.py -s` 

The "-s" flag makes the output more verbose and python print statements work.

## Features
The following auction types are implemented:
- English Auction (open first/second-price)
- Vickrey Auction (sealed first/second-price)
- Ebay-like automatic bidding
- Dutch Auction
- American Auction 
- Double Auction 
- Lottery

**Details in the report**