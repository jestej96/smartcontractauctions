# Some useful stuff 

## Structure
It is a good guideline to structure functions that interact
with other contracts (i.e. they call functions or send Ether)
into three phases:
1. checking conditions
2. performing actions (potentially changing conditions)
3. interacting with other contracts

If these phases are mixed up, the other contract could call
back into the current contract and modify the state or cause
effects (ether payout) to be performed multiple times.
If functions called internally include interaction with external
contracts, they also have to be considered interaction with
external contracts.

[Style guide](https://docs.soliditylang.org/en/v0.8.14/style-guide.html)

***
## Natspec Comments
The triple-slash comments are so-called natspec comments. They will be shown when the user is asked to confirm a transaction or when an error is displayed.

https://docs.soliditylang.org/en/latest/natspec-format.html

***
## revert vs require vs assert 

https://medium.com/blockchannel/the-use-of-revert-assert-and-require-in-solidity-and-the-new-revert-opcode-in-the-evm-1a3a7990e06e


revert()
- Handle the same type of situations as require(), but with more complex logic.
- Can be used with custom error types and dev commments


require() 
- validate user inputs ie. require(input<20);
- Validate the response from an external contract ie. require(external.send(amount));
- Validate state conditions prior to execution, ie. require(block.number > SOME_BLOCK_NUMBER) or require(balance[msg.sender]>=amount)
- Generally, you should use require most often
- Generally, it will be used towards the beginning of a function
- Can't be used with custom error types but with message and dev comment

assert()
- Check for overflow/underflow, ie. c = a+b; assert(c > b)
- Check invariants, ie. assert(this.balance >= totalSupply);
- Validate state after making changes
- Prevent conditions which should never, ever be possible
- Generally, you will probably use assert less often
- Generally, it will be used towards the end of a function.

***
## Withdrawal pattern
The withdrawal pattern places the responsibility for claiming funds, on the recipient of the funds: the recipient has to send a transaction to withdraw and obtain their funds.

See: https://docs.soliditylang.org/en/v0.8.15/common-patterns.html#withdrawal-from-contracts
*** 
