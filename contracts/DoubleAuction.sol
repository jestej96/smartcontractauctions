// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/access/Ownable.sol";

// A double auction is a process of buying and selling goods with
// multiple sellers and multiple buyers. Potential buyers submit
// their bids and potential sellers submit their ask prices to the
// market institution, and then the market institution chooses some
// price p that clears the market. How the price is determined
// depends on the mechanism used. In this contract
// The auctioneer decides which mechanism to use for clearance.
// Available mechanisms are: VCG, Trade Reduction and Mcafee's.

// Assumptions:
// each buyer or seller can only ask or offer
// sellers all sell the same arbitrary item and all sell one unit
// #sellers = #buyers
// Sealed bids are actually visible

// For simplicity, I removed the probabilistic trade reduction mechanism
// How the VRF Coordinator works is shown in other contracts.
// It would work like that:
// When probabilistic trade reduction mechanism is chosen,
// request a random number using the VRF Coordinator.
// Then, given a p∈[0,1], use the  trade reduction
// mechanism with probability p and the VCG mechanism
// with probability 1-p

// Time: based on block.number
// Bid visibility: sealed

contract DoubleAuction is Ownable {
    struct Commit {
        bytes32 cypher;
        uint256 deposit;
    }
    struct Revelation {
        address sender;
        uint256 bid;
    }
    enum Mechanism {
        VCG,
        TRADE_REDUCTION,
        MCAFEE
    }

    uint256 public immutable biddingEndBlock;
    uint256 public immutable revealEndBlock;

    mapping(address => Commit) public buyer_commits; // bid
    mapping(address => Commit) public seller_commits; // ask

    Revelation[] public buyer_reveals;
    Revelation[] public sellers_reveals;

    uint256 public buffer = 0.5 * 10**18; // Default buffer of 0.5 ETH

    /// @notice Create the double auction contract
    /// The auctioneer needs to make a deposit to subsidizes f.e. VCG.
    /// @param _biddingBlocks # blocks to allow committing bids/asks for
    /// @param _revealBlocks # blocks to allow reveal bids/asks for
    constructor(uint256 _biddingBlocks, uint256 _revealBlocks) payable {
        require(
            msg.value >= 10 * 10**18,
            "Auctioneer deposit of >= 10 ETH required"
        );
        biddingEndBlock = block.number + _biddingBlocks;
        revealEndBlock = biddingEndBlock + _revealBlocks;
    }

    /// @notice Place a sealed bid with
    /// `cypher` = keccak256(abi.encodePacked(value, secret)).
    /// The ether send with this transaction must be >= bid + buffer.
    /// Otherwise the bid is invalid and refunded after revealing it.
    /// Only possible during the bidding period.
    /// @param _cypher the encrypted ask
    function bid(bytes32 _cypher)
        external
        payable
        onlyBefore(biddingEndBlock)
        onlyOneBid
    {
        buyer_commits[msg.sender] = Commit({
            cypher: _cypher,
            deposit: msg.value
        });
    }

    /// @notice Place a sealed ask with
    /// `cypher` = keccak256(abi.encodePacked(value, secret)).
    /// Only possible during the bidding period.
    /// @param _cypher the encrypted ask
    function ask(bytes32 _cypher)
        external
        onlyBefore(biddingEndBlock)
        onlyOneBid
    {
        seller_commits[msg.sender] = Commit({cypher: _cypher, deposit: 0});
        // NOTE: would need to transfer item to contract here too
    }

    /// @notice Reveal a bid commit.
    /// Each revealed commit is pushed into the buyer_commits array
    /// as a Revelation object.
    /// Funds greater then the actual bid + buffer are refunded.
    /// Commits that are not revealed aren't refunded.
    /// @param _value the actual value of the bid
    /// @param _secret the bytes32 secret used for encrypting the bid
    function revealBid(uint256 _value, bytes32 _secret)
        external
        onlyAfter(biddingEndBlock)
        onlyBefore(revealEndBlock)
    {
        Commit storage bidToCheck = buyer_commits[msg.sender];

        if (bidToCheck.cypher != keccak256(abi.encodePacked(_value, _secret))) {
            // Bid was not actually revealed.
            // Do not refund deposit.
            return;
        }

        require(bidToCheck.deposit >= _value + buffer);

        // make it impossible for the sender to re-claim the same deposit.
        bidToCheck.cypher = bytes32(0);

        // save the revealed values into buyer_reveals array
        buyer_reveals.push(Revelation({sender: msg.sender, bid: _value}));

        // transfer exceeding funds
        uint256 refund = bidToCheck.deposit - _value - buffer;
        if (refund > 0) payable(msg.sender).transfer(refund);
    }

    /// @notice Reveal an ask commit.
    /// Each revealed commit is pushed into the seller_commits array
    /// as a Revelation object.
    /// @param _value the actual value of the ask
    /// @param _secret the bytes32 secret used for encrypting the ask
    function revealAsk(uint256 _value, bytes32 _secret)
        external
        onlyAfter(biddingEndBlock)
        onlyBefore(revealEndBlock)
    {
        if (
            seller_commits[msg.sender].cypher !=
            keccak256(abi.encodePacked(_value, _secret))
        ) {
            // Ask was not actually revealed.
            return;
        }

        // make it impossible to reveal again
        seller_commits[msg.sender].cypher = bytes32(0);

        // save the revealed values into seller_reveals array
        sellers_reveals.push(Revelation({sender: msg.sender, bid: _value}));
    }

    /// @notice After the buyers and sellers revealed their bids/ask
    /// the owner of the contract calls this method to "clear" the
    /// market by the given mechanism.
    /// @param _clearance_mechanism clearance mechanism to use for clearance
    /// @return buyers_desc array of revealed buyers order descending (b)
    /// @return sellers_asc array of revealed sellers order ascending (s)
    /// @return bei the break even index (k)
    /// @return pay the price payed by buyers determined my the mechanism
    /// @return get the price received by sellers determined my the mechanism
    function clearance(Mechanism _clearance_mechanism)
        external
        onlyAfter(revealEndBlock)
        onlyOwner
        returns (
            Revelation[] memory,
            Revelation[] memory,
            uint256 bei,
            uint256 pay,
            uint256 get
        )
    {
        require(
            buyer_reveals.length == sellers_reveals.length,
            "Currently only the same number of buyer_commits and seller_commits is supported!"
        );

        // copy buyer_reveals
        Revelation[] memory buyers_desc = buyer_reveals;

        // sort buyers descending
        quickSortOpenBids(
            buyers_desc,
            0,
            int256(buyers_desc.length - 1),
            false
        );

        // copy sellers_reveals
        Revelation[] memory sellers_asc = sellers_reveals;

        // sort sellers ascending
        quickSortOpenBids(sellers_asc, 0, int256(sellers_asc.length - 1), true);

        bei = getBEI(buyers_desc, sellers_asc);

        if (_clearance_mechanism == Mechanism.VCG) {
            (pay, get) = clear_vcg(buyers_desc, sellers_asc, bei);
        } else if (_clearance_mechanism == Mechanism.TRADE_REDUCTION) {
            (pay, get) = clear_trade_reduction(buyers_desc, sellers_asc, bei);
        } else if (_clearance_mechanism == Mechanism.MCAFEE) {
            (pay, get) = clear_mcafee(buyers_desc, sellers_asc, bei);
        } else {
            revert("Mechanism not found");
        }

        return (buyers_desc, sellers_asc, bei, pay, get);
    }

    /// @notice Calculate the break even index of the given
    /// ordered arrays of buyers and sellers.
    /// To do so first create the natural ordering:
    /// Order the buyers in decreasing order of their bid
    /// Order the sellers in increasing order of their bid
    /// Let BEI be the largest index such that buyer k ≥ seller k
    /// Means ->  at the break-even index (k), the buyer price is
    /// still higher or the same as the seller price.
    /// @dev Limitations: only same length of buyers and sellers supported.
    /// Also the break even index can't be at the last array element.
    /// @param _buyers_desc array of revealed buyers order descending
    /// @param _sellers_asc array of revealed sellers order ascending
    /// @param bei the break even index (k)
    function getBEI(
        Revelation[] memory _buyers_desc,
        Revelation[] memory _sellers_asc
    ) internal pure returns (uint256 bei) {
        for (uint256 i = 0; i < _buyers_desc.length; i++) {
            if (_buyers_desc[i].bid < _sellers_asc[i].bid) {
                require(i > 0, "Break-even index 0 not supported");
                bei = i - 1;
                break;
            }
        }
        require(bei > 0, "Break-even index at end not supported");

        return bei;
    }

    /// @notice Clear the market using the VCG mechanism.
    /// In the general double auction setting, the mechanism orders
    /// the buyers and sellers in the Natural ordering and finds the
    /// breakeven index k. Then the first k sellers give the item to
    /// the first k buyers. Each buyer pays the lowest equilibrium
    /// price max(sk,bk+1), and each seller receives the highest
    /// equilibrium price min(bk,sk+1).
    /// See VCG mechanism at https://en.wikipedia.org/wiki/Double_auction
    /// @param _buyers_desc array of revealed buyers order descending (b)
    /// @param _sellers_asc array of revealed sellers order ascending (s)
    /// @param _bei the break even index (k)
    /// @return pay the price payed by buyers determined my the mechanism
    /// @return get the price received by sellers determined my the mechanism
    function clear_vcg(
        Revelation[] memory _buyers_desc,
        Revelation[] memory _sellers_asc,
        uint256 _bei
    ) public returns (uint256 pay, uint256 get) {
        // determine what buyers pay and what sellers get
        if (_sellers_asc[(_bei) + 1].bid > _buyers_desc[(_bei)].bid) {
            if (_buyers_desc[(_bei + 1)].bid < _sellers_asc[(_bei)].bid) {
                pay = _sellers_asc[(_bei)].bid;
                get = _buyers_desc[(_bei)].bid;
            } else {
                pay = _buyers_desc[(_bei + 1)].bid;
                get = _buyers_desc[(_bei)].bid;
            }
        } else {
            if (_buyers_desc[(_bei + 1)].bid < _sellers_asc[(_bei)].bid) {
                pay = _sellers_asc[(_bei)].bid;
                get = _sellers_asc[(_bei + 1)].bid;
            } else {
                pay = _buyers_desc[(_bei)].bid;
                get = _sellers_asc[(_bei + 1)].bid;
            }
        }

        transferFunds(
            _buyers_desc,
            _sellers_asc,
            pay,
            get,
            _buyers_desc.length
        );

        return (pay, get);
    }

    /// @notice Clear the market using the VCG mechanism.
    /// The mechanism works as follows:
    /// (k is the break even index, bk is the buyer k, sk is the seller k)
    /// The first k-1 sellers give the item and receive sk from the auctioneer;
    /// The first k-1 buyers receive the item and pay bk to the auctioneer.
    /// @param _buyers_desc array of revealed buyers order descending (b)
    /// @param _sellers_asc array of revealed sellers order ascending (s)
    /// @param _bei the break even index (k)
    /// @return pay the price payed by buyers determined my the mechanism
    /// @return get the price received by sellers determined my the mechanism
    function clear_trade_reduction(
        Revelation[] memory _buyers_desc,
        Revelation[] memory _sellers_asc,
        uint256 _bei
    ) public returns (uint256 pay, uint256 get) {
        pay = _buyers_desc[_bei].bid;
        get = _sellers_asc[_bei].bid;

        transferFunds(_buyers_desc, _sellers_asc, pay, get, _bei);
        return (pay, get);
    }

    /// @notice Clear the market using McAfee's mechanism.
    /// The mechanism works as follows:
    /// (k is the break even index, bk is the buyer k, sk is the seller k)
    /// Calculate: p=(bk+1+sk+1)/2.
    /// If bk≥p≥sk, then the first k buyers and sellers trade the good in price p.
    /// Otherwise, the first k-1 sellers trade for sk and the first k-1 buyers trade
    /// for bk as in the trade-reduction mechanism.
    /// @param _buyers_desc array of revealed buyers order descending (b)
    /// @param _sellers_asc array of revealed sellers order ascending (s)
    /// @param _bei the break even index (k)
    /// @return pay the price payed by buyers determined my the mechanism
    /// @return get the price received by sellers determined my the mechanism
    function clear_mcafee(
        Revelation[] memory _buyers_desc,
        Revelation[] memory _sellers_asc,
        uint256 _bei
    ) public returns (uint256 pay, uint256 get) {
        pay = (_buyers_desc[_bei + 1].bid + _sellers_asc[_bei + 1].bid) / 2;
        get = pay;
        if (_buyers_desc[_bei].bid >= pay && pay >= _sellers_asc[_bei].bid) {
            transferFunds(_buyers_desc, _sellers_asc, pay, get, _bei + 1);
        } else {
            (pay, get) = clear_trade_reduction(
                _buyers_desc,
                _sellers_asc,
                _bei
            );
        }

        return (pay, get);
    }

    /// @notice All clearance mechanisms use this function
    /// to actually do the transactions after determining the
    /// buyer and seller prices.
    /// @param _buyers_desc array of revealed buyers order descending (b)
    /// @param _sellers_asc array of revealed sellers order ascending (s)
    /// @param _pay the price to pay by buyers determined my the mechanism
    /// @param _get the price to transfer to sellers determined my the mechanism
    /// @param _transferStopIndex only transfer for buyers/sellers smaller
    /// than that index (necessary for some mechanisms).
    function transferFunds(
        Revelation[] memory _buyers_desc,
        Revelation[] memory _sellers_asc,
        uint256 _pay,
        uint256 _get,
        uint256 _transferStopIndex
    ) internal {
        // check if contract balance is sufficient
        require(
            address(this).balance >= _transferStopIndex * _get,
            "Not enough funds to subsidizes."
        );

        // transfer funds to sellers
        for (uint256 i = 0; i < _transferStopIndex; i++) {
            payable(_sellers_asc[i].sender).transfer(_get);
        }

        // possibly refund buyers
        // NOTE: you would need to transfer the items here too
        // NOTE: would need to send back the items for not involved sellers here
        for (uint256 i = 0; i < _buyers_desc.length; i++) {
            uint256 refund = _buyers_desc[i].bid + buffer;
            if (i < _transferStopIndex) {
                refund -= _pay;
            }
            payable(_buyers_desc[i].sender).transfer(refund);
        }
    }

    /// @notice Quick sort given array of Revelations ascending or descending.
    /// @param _arr array of Revelations to sort
    /// @param _left used internally for left index
    /// give 0 at first call to sort complete array.
    /// @param _right used internally for right index
    /// give array.length - 1 at first call to sort complete array.
    /// @param _asc true -> ascending, false -> descending
    function quickSortOpenBids(
        Revelation[] memory _arr,
        int256 _left,
        int256 _right,
        bool _asc
    ) internal view {
        int256 i = _left;
        int256 j = _right;
        if (i == j) return;
        uint256 pivot = _arr[uint256(_left + (_right - _left) / 2)].bid;
        while (i <= j) {
            if (_asc) {
                while (_arr[uint256(i)].bid < pivot) i++;
                while (pivot < _arr[uint256(j)].bid) j--;
            } else {
                while (_arr[uint256(i)].bid > pivot) i++;
                while (pivot > _arr[uint256(j)].bid) j--;
            }

            if (i <= j) {
                (_arr[uint256(i)], _arr[uint256(j)]) = (
                    _arr[uint256(j)],
                    _arr[uint256(i)]
                );
                i++;
                j--;
            }
        }
        if (_left < j) quickSortOpenBids(_arr, _left, j, _asc);
        if (i < _right) quickSortOpenBids(_arr, i, _right, _asc);
    }

    /// @notice The contract owner can set the buffer here.
    /// @param _buffer the new buffer.
    function setBuffer(uint256 _buffer) external onlyOwner {
        buffer = _buffer;
    }

    modifier onlyBefore(uint256 _blockNumber) {
        if (block.number >= _blockNumber) revert();
        _;
    }
    modifier onlyAfter(uint256 _blockNumber) {
        if (block.number <= _blockNumber) revert();
        _;
    }

    modifier onlyOneBid() {
        require(buyer_commits[msg.sender].cypher == 0);
        require(seller_commits[msg.sender].cypher == 0);
        _;
    }
}
