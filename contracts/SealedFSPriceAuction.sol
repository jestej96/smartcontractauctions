// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

// This sealed auction format works with first and second price.
// Just choose the type at construction.
// For mainnet deployment only one type should be included to reduce gas costs.

// In the sealed first/second price auction, all bidders simultaneously
// submit sealed bids during a bidding period.
// During the following reveal phase, the bidders need to reveal their previous encrypted bids.
// In the first price auction, the highest bidder pays the price he/she submitted.
// In the second price auction, the highest bidder pays the price bid by the second-highest bidder

// Beneficiary: contract creator
// Time: based on block.number
// Bid visibility: sealed
// Bid gathering: no, only the committed bids are used

contract SealedFSPriceAuction {
    struct SealedBid {
        bytes32 cypher;
        uint256 deposit;
    }

    address payable public immutable beneficiary;
    uint256 public immutable biddingEndBlock;
    uint256 public immutable revealEndBlock;
    bool public auctionEnded;

    mapping(address => SealedBid[]) public bids;

    address public highestBidder;
    uint256 public highestBid;

    // for second price type
    bool public immutable secondPrice;
    uint256 public secondHighestBid;

    // Allowed withdrawals of previous bids
    mapping(address => uint256) pendingReturns;

    event LogBid(address bidder, bytes32 cypher);
    event AuctionEnded(address winner, uint256 highestBid);

    // Errors that describe failures.
    /// The function has been called too early.
    /// Try again at block `number`.
    error TooEarly(uint256 number);
    /// The function has been called too late.
    /// It cannot be called after block `number`.
    error TooLate(uint256 number);
    /// The function endAuction has already been called.
    error AuctionEndAlreadyCalled();

    constructor(
        uint256 _biddingBlocks,
        uint256 _revealBlocks,
        bool _secondPrice
    ) {
        require(0 < _biddingBlocks, "Bidding period block count must be > 0");
        biddingEndBlock = block.number + _biddingBlocks;
        revealEndBlock = biddingEndBlock + _revealBlocks;
        secondPrice = _secondPrice;
        beneficiary = payable(msg.sender);
        // NOTE: would need to transfer item to the contract here
    }

    /// @notice Place a sealed bid with `cypher` =
    /// keccak256(abi.encodePacked(value, fake, secret)).
    /// Only possible during the bidding period.
    /// The ether sent is only refunded if the bid is correctly
    /// revealed in the revealing phase. The bid is valid if the
    /// ether sent together with the bid is at least "value" and
    /// "fake" is not true. Setting "fake" to true and sending
    /// not the exact amount are ways to hide the real bid but
    /// still make the required deposit. The same address can
    /// place multiple bids.
    /// @param _cypher the encrypted bid
    function bid(bytes32 _cypher) external payable onlyBefore(biddingEndBlock) {
        bids[msg.sender].push(SealedBid({cypher: _cypher, deposit: msg.value}));
        emit LogBid(msg.sender, _cypher);
    }

    /// @notice Reveal your blinded bids.
    /// Only possible during reveal period.
    /// You will get a refund for all correctly blinded invalid
    /// bids and for all bids except for the totally highest.
    /// All committed bids need to be revealed.
    /// @param _values array of values bidden
    /// @param _fakes array of true/false for fake bids
    /// @param _secrets array of secrets
    function reveal(
        uint256[] calldata _values,
        bool[] calldata _fakes,
        bytes32[] calldata _secrets
    ) external onlyAfter(biddingEndBlock) onlyBefore(revealEndBlock) {
        uint256 length = bids[msg.sender].length;
        require(_values.length == length);
        require(_fakes.length == length);
        require(_secrets.length == length);

        uint256 refund;
        // iterate all revelations to place valid bids and
        // calculate the refunds
        for (uint256 i = 0; i < length; i++) {
            SealedBid storage bidToCheck = bids[msg.sender][i];
            (uint256 value, bool fake, bytes32 secret) = (
                _values[i],
                _fakes[i],
                _secrets[i]
            );
            // check if bid was actually revealed.
            if (
                bidToCheck.cypher !=
                keccak256(abi.encodePacked(value, fake, secret))
            ) {
                // It was not => do not refund deposit.
                continue;
            }
            refund += bidToCheck.deposit;
            if (!fake && bidToCheck.deposit >= value) {
                if (placeBid(msg.sender, value)) refund -= value;
            }
            // Make it impossible for the sender to re-claim
            // the same deposit.
            bidToCheck.cypher = bytes32(0);
        }
        payable(msg.sender).transfer(refund);
    }

    /// @notice Withdraw a bid that was overbid.
    /// Unrevealed bids can't be revealed ever.
    function withdraw() external {
        uint256 amount = pendingReturns[msg.sender];
        if (amount > 0) {
            // It is important to set this to zero because the recipient
            // can call this function again as part of the receiving call
            // before `transfer` returns (see the remark above about
            // conditions -> effects -> interaction).
            pendingReturns[msg.sender] = 0;

            payable(msg.sender).transfer(amount);
        }
    }

    /// @notice End the auction and send the highest bid
    /// to the beneficiary. If second price is used make
    /// the difference of highest and second highest
    /// price available for withdrawal.
    function endAuction() external onlyAfter(revealEndBlock) {
        if (auctionEnded) revert AuctionEndAlreadyCalled();

        uint256 price = highestBid;
        if (secondPrice && secondHighestBid > 0) {
            price = secondHighestBid;
            pendingReturns[highestBidder] += highestBid - secondHighestBid;
        }
        emit AuctionEnded(highestBidder, price);
        auctionEnded = true;
        beneficiary.transfer(price);
        // NOTE: would need to transfer item to highestBidder here
    }

    /// @notice Internal function for placing the revealed bids.
    /// It updates highestBid and highestBidder
    /// (and secondHighestBid) if the highest bid is overbid.
    /// @param _bidder the bidders address
    /// @param _value the value of the bid
    /// @return success true if new highest bid, else false
    function placeBid(address _bidder, uint256 _value)
        internal
        returns (bool success)
    {
        if (_value <= highestBid) {
            if (_value > secondHighestBid) {
                secondHighestBid = _value;
            }
            return false;
        }
        if (highestBidder != address(0)) {
            // Make refund for the previously highest bidder available
            pendingReturns[highestBidder] += highestBid;
        }
        secondHighestBid = highestBid;
        highestBid = _value;
        highestBidder = _bidder;
        return true;
    }

    modifier onlyBefore(uint256 number) {
        if (block.number >= number) revert TooLate(number);
        _;
    }
    modifier onlyAfter(uint256 number) {
        if (block.number <= number) revert TooEarly(number);
        _;
    }
}
