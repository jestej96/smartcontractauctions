// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@chainlink/contracts/src/v0.8/VRFConsumerBase.sol";

// This is an ALL-PAY format American auction.
// Anyone who submits a bid pays the difference to the previous bid.
// To remove withdrawal transactions, this auction just accepts bids
// higher than the given minimum bid. Then the winner is just the
// bidder who bid last. Hence, bidders still just pay the difference.

// To make it a bit more interesting, I added a probabilistic winning mechanism.
// When creating the contract, the bidding period is given in blocks as usual.
// Whenever a bid is made, there is a probability of winning the auction.
// This probability rises linearly to 50% when the bidding period ends.
// If the auction didn't end yet, the auction just continues and the probability
// of winning keeps on growing to a maximum of 90%.
// In production, this method is probably too costly. An idea would be to
// reduce the VRF transactions - maybe by gathering bids and requesting
// multiple random numbers at once using the VRF v.2.

// At any time, only the auctioneer/beneficiary can withdraw the collected funds.
// To pick the winner the contract requests a random number using Chainlink's VRF
// on every bid. It calculates the current winning probability and checks if
// the random number falls into the winning interval. If so, the auction ends
// and the winner is announced.

// Beneficiary: contract creator
// Time: based on block.number
// Bid visibility: open

contract AmericanAuction is VRFConsumerBase {
    address public immutable beneficiary;
    uint256 public immutable startBlock;
    uint256 public immutable biddingBlocks;
    uint256 public immutable minBid;
    uint256 public immutable startProba; // 18 decimals
    address public latestBidder;

    // Necessary for VRFCoordinator:
    uint256 public immutable fee;
    bytes32 public immutable keyhash;
    uint256 public randomness;

    bool public auctionEnded = false;

    mapping(bytes32 => address) requestsByBidder;
 
    event LogBid(address bidder, uint256 totalFunds);
    event BadLuck(address bidder);
    event WinnerPicked(address winner, uint256 totalFunds);
    event RequestedRandomness(bytes32 requestId);

    /// @notice Create the auction. Beneficiary set to constructor caller
    /// and the auction starts with this transaction.
    /// @param _biddingBlocks the duration to accept bids, in blocks
    /// @param _minBid the minimum bid to accept
    /// @param _startProba probability of winning at auction start (18 decimals)
    /// @param _vrfCoordinator contract address for VRF Coordinator
    /// @param _link address of the link token
    /// @param _fee LINK fee of the vrfCoordinator
    /// @param _keyhash used to uniquely identify the VRF Coordinator
    constructor(
        uint256 _biddingBlocks,
        uint256 _minBid,
        uint256 _startProba,
        address _vrfCoordinator,
        address _link,
        uint256 _fee,
        bytes32 _keyhash
    ) VRFConsumerBase(_vrfCoordinator, _link) {
        startBlock = block.number;
        biddingBlocks = _biddingBlocks;
        minBid = _minBid;
        beneficiary = payable(msg.sender);
        startProba = _startProba;
        fee = _fee;
        keyhash = _keyhash;
        // NOTE: would need to transfer item to the contract here
    }

    /// @notice Send a bid to the auction.
    /// After every bid there is a probability of
    /// winning and ending the auction.
    /// The probability grows linear with the blocks
    /// mined since the startBlock.
    function bid() external payable {
        require(!auctionEnded, "Winner already picked");

        require(
            msg.value >= minBid,
            "Bid needs to be higher then the minimum bid"
        );

        require(
            LINK.balanceOf(address(this)) >= fee,
            "Not enough LINK - fill contract with faucet."
        );

        emit LogBid(msg.sender, address(this).balance);

        bytes32 requestId = requestRandomness(keyhash, fee);
        requestsByBidder[requestId] = msg.sender;

        emit RequestedRandomness(requestId);
        latestBidder = msg.sender;
    }

    /// @notice Callback function used by VRF Coordinator.
    /// The winning probability is calculated and tested with the
    /// random number. In case of winning the auction ends.
    /// @param _requestId id of the request
    /// @param _randomness the uint256 random number
    function fulfillRandomness(bytes32 _requestId, uint256 _randomness)
        internal
        override
    {
        require(_randomness > 0, "random-not-found");
        require(!auctionEnded, "Winner already picked");

        randomness = _randomness;

        uint256 blocksPassed = block.number - startBlock;

        // calculate the winning probability depending on passed blocks
        uint256 proba = ((0.5 * 10**18 - startProba) / biddingBlocks) *
            blocksPassed +
            startProba; // 18 decimals

        // maximum winning probability of 90%
        if (proba > 0.9 * 10**18) {
            proba = 0.9 * 10**18;
        }

        address bidder = requestsByBidder[_requestId];

        // get a random number in [1, 10**18]
        uint256 randomResult = (_randomness % 10**18) + 1;

        // check if the randomResult is inside the current winning interval
        if (proba >= randomResult) {
            auctionEnded = true;
            latestBidder = bidder;
            emit WinnerPicked(bidder, address(this).balance);
            // NOTE: would need to transfer item to the bidder here
        } else {
            emit BadLuck(bidder);
        }
    }

    /// @notice owner can withdraw contract funds
    function withdrawFunds() external onlyBeneficiary {
        payable(beneficiary).transfer(address(this).balance);
    }

    /// @return funds the collected funds
    function getFunds() external view onlyBeneficiary returns (uint256) {
        return address(this).balance;
    }

    modifier onlyBeneficiary() {
        require(
            msg.sender == beneficiary,
            "Only the beneficiary can call this function."
        );
        _;
    }
}
