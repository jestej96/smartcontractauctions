// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@chainlink/contracts/src/v0.8/interfaces/AggregatorV3Interface.sol";

// This is an eBay-like auction format with automatic bidding.
// All bidders bid simultaneously during a bidding period.
// It uses bid increments depending on the current price (same as eBay).
// The contract bids in increments on your behalf to keep you in the
// lead but only up to your limit.

// In this format the highest bid should be invisible for other bidders.
// Since this would require many useless transactions or some kind of order
// preserving encryption, bids are open for simplicity.

// To try the usage of Chainlink's data feeds an AggregatorV3Interface is used
// to convert ETH to USD. The price steps and bid increments are taken directly
// from eBay, hence they are in USD. When a bid is made the increment in ETH is
// calculated by (live course) converting the bid into USD and back.

// Beneficiary: contract creator
// Time: based on block.number
// Bid visibility: open
// Bid gathering: all overbid funds are combined to a new bid.
//                Bidders can also choose to withdraw overbid funds.

contract AutomaticBiddingAuction {
    address payable public immutable beneficiary;
    uint256 public immutable biddingEndBlock;

    AggregatorV3Interface internal ethUsdPriceFeed;

    uint256 public highestBindingBid;
    address public highestBidder;
    mapping(address => uint256) public fundsByBidder;

    bool beneficiaryHasWithdrawn;

    // no decimals
    uint16[10] public priceSteps = [
        0,
        1,
        5,
        25,
        100,
        250,
        500,
        1000,
        2500,
        5000
    ];

    // two decimals
    uint16[10] public bidIncrements = [
        5,
        25,
        50,
        100,
        250,
        500,
        1000,
        2500,
        5000,
        10000
    ];

    event LogBid(
        address bidder,
        uint256 bid,
        address highestBidder,
        uint256 highestBid,
        uint256 highestBindingBid,
        uint256 bidIncrement
    );
    event LogWithdrawal(
        address withdrawer,
        address withdrawalAccount,
        uint256 amount
    );

    /// @notice Create an auction where the startBlock is the block on
    /// which the contract was created on and beneficiary is the contract creator.
    /// @param _biddingBlocks number of blocks to allow bidding for.
    /// @param _priceFeedAddress contract address for AggregatorV3Interface
    constructor(uint256 _biddingBlocks, address _priceFeedAddress) {
        require(0 < _biddingBlocks, "Bidding period block count must be > 0");
        biddingEndBlock = block.number + _biddingBlocks;
        beneficiary = payable(msg.sender);
        ethUsdPriceFeed = AggregatorV3Interface(_priceFeedAddress);
    }

    /// @notice Calculate the bid increment depending on the current price.
    /// @param _currentPrice the current highest binding bid
    /// @return bidIncrement in Wei (18 decimals)
    function getBidIncrement(uint256 _currentPrice)
        public
        view
        returns (uint256)
    {
        (, int256 price, , , ) = ethUsdPriceFeed.latestRoundData(); // 8 decimals

        // convert _currentPrice (Wei) to usd price
        uint256 usdPrice = (uint256(price) * _currentPrice) / 10**18; // 8 decimals

        // look for interval in priceSteps
        uint8 index = 0;
        for (; index < priceSteps.length; index++) {
            if (usdPrice < uint256(priceSteps[index]) * 10**8) break;
        }

        // convert the intervals bidIncrement from USD to Wei
        return (uint256(bidIncrements[index - 1]) * 10**24) / uint256(price); // 18 decimals
    }

    /// @notice Make a new bid with the value send with this transaction.
    /// The bid must be greater then the current highest binding bid + bid increment.
    /// Funds that are greater than the new highest binding bid are used for auto bidding.
    function bid()
        external
        payable
        onlyBefore(biddingEndBlock)
        onlyNotBeneficiary
    {
        // reject payments of 0 ETH
        require(msg.value > 0, "Payments of 0 ETH are invalid");

        // calculate the bidder's total bid
        uint256 newBid = fundsByBidder[msg.sender] + msg.value;

        // get the bid increment depending on the current highestBindingBid
        uint256 bidIncrement = getBidIncrement(highestBindingBid);

        require(
            newBid >= highestBindingBid + bidIncrement,
            "Bid must be greater then highest binding bid + bid increment"
        );

        uint256 highestBid = fundsByBidder[highestBidder];

        fundsByBidder[msg.sender] = newBid;

        if (newBid <= highestBid) {
            // the bidder overbid the highestBindingBid but not the highestBid
            // => increase the highestBindingBid, highestBidder doens't change
            highestBindingBid = min(newBid + bidIncrement, highestBid);
        } else {
            // if msg.sender is already the highest bidder, they must simply be wanting to raise
            // their maximum bid, in which case we should NOT increase the highestBindingBid.

            // if the user is NOT highestBidder, and has overbid highestBid completely, we set them
            // as the new highestBidder and recalculate highestBindingBid.

            if (msg.sender != highestBidder) {
                highestBidder = msg.sender;
                highestBindingBid = min(newBid, highestBid + bidIncrement);
            }
            highestBid = newBid;
        }

        emit LogBid(
            msg.sender,
            msg.value,
            highestBidder,
            highestBid,
            highestBindingBid,
            bidIncrement
        );
    }

    /// @notice Multipurpose withdraw method.
    /// The beneficiary can withdraw the highestBindingBid
    /// after the auction ended (and only once of cause).
    /// The highestBidder can withdraw remaining funds after
    /// after the auction ended (and only once of cause).
    /// Every other bidder can withdraw his/her overbid funds any time.
    function withdraw() external {
        address withdrawalAccount;
        uint256 withdrawalAmount;

        if (msg.sender == beneficiary) {
            // If the auction ended the beneficiary should
            // be allowed to withdraw the highestBindingBid
            require(
                block.number > biddingEndBlock,
                "The auction has not ended yet."
            );

            require(
                !beneficiaryHasWithdrawn,
                "Beneficiary already withdrawed."
            );

            withdrawalAccount = highestBidder;
            withdrawalAmount = highestBindingBid;
            beneficiaryHasWithdrawn = true;
            // NOTE: the highestBidder must be able to "withdraw" the item
        } else if (msg.sender == highestBidder) {
            // The highest bidder should only be allowed to withdraw
            // if the auction anded.
            // Then the highest bidder can withdraw the difference
            // between its highest bid and the highest binding bid
            require(
                block.number > biddingEndBlock,
                "The auction has not ended yet."
            );
            withdrawalAccount = highestBidder;
            if (beneficiaryHasWithdrawn) {
                withdrawalAmount = fundsByBidder[highestBidder];
            } else {
                withdrawalAmount =
                    fundsByBidder[highestBidder] -
                    highestBindingBid;
            }
        } else {
            // every overbid funds can we withdraw anytime
            withdrawalAccount = msg.sender;
            withdrawalAmount = fundsByBidder[withdrawalAccount];
        }

        require(withdrawalAmount > 0, "There are no funds to withdraw.");

        fundsByBidder[withdrawalAccount] -= withdrawalAmount;
        payable(msg.sender).transfer(withdrawalAmount);

        emit LogWithdrawal(msg.sender, withdrawalAccount, withdrawalAmount);
    }

    function min(uint256 _a, uint256 _b) private pure returns (uint256) {
        if (_a < _b) return _a;
        return _b;
    }

    modifier onlyBefore(uint256 _number) {
        if (block.number >= _number) revert("The auction has already ended.");
        _;
    }
    modifier onlyAfter(uint256 _number) {
        if (block.number <= _number) revert("The auction has not ended yet.");
        _;
    }

    modifier onlyNotBeneficiary() {
        require(
            msg.sender != beneficiary,
            "The beneficiary can't call this function."
        );
        _;
    }
}
