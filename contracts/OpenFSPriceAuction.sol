// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

// This open auction format works with first and second price.
// Just choose the type at construction.
// For mainnet deployment only one type should be included to reduce gas costs.

// In the open first/second price auction all bidders simultaneously
// submit open bids during a bidding period.
// In the first price auction the highest bidder pays the price he/she submitted.
// In the second price auction the highest bidder pays the price
// bid by the second-highest bidder

// Beneficiary: contract creator
// Time: based on block.timestamp
// Bid visibility: open
// Bid gathering: all overbid funds are added to a new bid.
//                Bidders can also choose to withdraw overbid funds.

// block.timestamp = timestamp of the current block
// in seconds since the epoch (seconds since 1970-01-01)

contract OpenFSPriceAuction {
    address public immutable beneficiary;
    uint256 public immutable endTime;

    uint256 public immutable startingPrice;
    address public highestBidder;
    mapping(address => uint256) public fundsByBidder;

    // set to true at the end => disallows any change.
    bool auctionEnded = false;

    // additions to allow second price instead of first price
    bool public immutable secondPrice;
    address public secondHighestBidder;

    // events that will be emitted on changes.
    event HighestBidIncreased(address bidder, uint256 bid, uint256 highestBid);
    event AuctionEnded(address winner, uint256 amount);

    /// @notice Create an auction with caller as beneficiary.
    /// The auction starts with calling the constructor.
    /// @param _startingPrice price to start with
    /// @param _biddingTime the duration to accept bids, in seconds
    /// @param _secondPrice true = use second price, false = first price
    constructor(
        uint256 _startingPrice,
        uint256 _biddingTime,
        bool _secondPrice
    ) {
        startingPrice = _startingPrice;
        beneficiary = msg.sender;
        endTime = block.timestamp + _biddingTime;
        secondPrice = _secondPrice;
        // NOTE: would need to transfer item to the contract here
    }

    /// @notice Bid the value sent together with this transaction.
    /// Funds of sender still in contract are added.
    /// Therefor, sender only need to bid the difference
    /// of the highest bid and their funds.
    function bid() external payable onlyBefore(endTime) {
        // Bid must be higher then the highest bid
        uint256 newBid = fundsByBidder[msg.sender] + msg.value;

        require(
            newBid >= startingPrice,
            "Bid must be greater then the starting price."
        );

        require(
            newBid > fundsByBidder[highestBidder],
            "There is already a higher or equal bid."
        );

        // overbid -> update highestBidder and funds
        fundsByBidder[msg.sender] = newBid;

        // memorize the second highest bidder if second price type
        if (secondPrice) {
            secondHighestBidder = highestBidder;
        }

        highestBidder = msg.sender;

        emit HighestBidIncreased(msg.sender, msg.value, newBid);
    }

    /// @notice Withdraw overbid funds.
    /// @return bool withdrawal successful?
    function withdraw() external returns (bool) {
        require(
            msg.sender != highestBidder || auctionEnded,
            "Highest bidder can't withdraw during bidding period."
        );
        uint256 amount = fundsByBidder[msg.sender];
        if (amount > 0) {
            // set the remaining returns to 0
            fundsByBidder[msg.sender] = 0;
            payable(msg.sender).transfer(amount);
        }
        return true;
    }

    /// @notice End the auction and send the highest bid to the beneficiary.
    function auctionEnd() external onlyAfter(endTime) {
        require(
            !auctionEnded,
            "The function auctionEnd has already been called."
        );

        auctionEnded = true;

        // determine the price based on first or second price type
        uint256 price = fundsByBidder[highestBidder];
        if (secondPrice && secondHighestBidder != address(0)) {
            price = fundsByBidder[secondHighestBidder];
        }

        // subtract the price from the funds of the highest bidder
        fundsByBidder[highestBidder] -= price;

        payable(beneficiary).transfer(price);
        // NOTE: would need to transfer item to highestBidder here

        emit AuctionEnded(highestBidder, price);
    }

    modifier onlyBefore(uint256 time) {
        require(block.timestamp < time, "The auction has already ended.");
        _;
    }
    modifier onlyAfter(uint256 time) {
        require(block.timestamp > time, "The auction has not ended yet.");
        _;
    }
}
