// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@chainlink/contracts/src/v0.8/VRFConsumerBase.sol";

// This is an Lottery contract.
// During the entering period, everybody can enter the lottery
// by transferring the defined entry fee.
// Users can also buy multiple "tickets".

// To pick the winner the contract requests a random number using
// Chainlink's VRF after the entering period ended.
// After picking the winner, the lottery starts over again.
// The winner receives the total funds collected.

// Alternatively, we could send the winner an item and
// send the funds to the beneficiary, if we want to
// give away an item like in the auction contacts.

// Time: based on block.number
// Visibility: open, everybody can see who entered

contract Lottery is VRFConsumerBase {
    uint256 public immutable entryFee;
    address payable[] public players;
    address payable public recentWinner;
    uint256 public randomness;

    uint256 public immutable enteringPeriod;
    uint256 public endBlock;

    enum LOTTERY_STATE {
        OPEN,
        CALCULATING_WINNER
    }
    LOTTERY_STATE public lottery_state;
    uint256 public immutable fee;
    bytes32 public immutable keyhash;

    event RequestedRandomness(bytes32 requestId);

    /// @notice Create the lottery contract
    /// @param _entryFee lottery "ticket" price in Wei
    /// @param _vrfCoordinator contract address for VRF Coordinator
    /// @param _link address of the link token
    /// @param _fee LINK fee of the vrfCoordinator
    /// @param _keyhash used to uniquely identify the VRF Coordinator
    constructor(
        uint256 _entryFee,
        uint256 _enteringPeriod,
        address _vrfCoordinator,
        address _link,
        uint256 _fee,
        bytes32 _keyhash
    ) VRFConsumerBase(_vrfCoordinator, _link) {
        entryFee = _entryFee;
        enteringPeriod = _enteringPeriod;
        endBlock = block.number + _enteringPeriod;
        lottery_state = LOTTERY_STATE.OPEN;
        fee = _fee;
        keyhash = _keyhash;
    }

    /// @notice Enter the lottery by transfering the entry fee
    function enter() public payable onlyBefore(endBlock) {
        require(lottery_state == LOTTERY_STATE.OPEN);
        require(msg.value == entryFee, "Not enough ETH!");
        players.push(payable(msg.sender));
    }

    /// @notice End the lottery after enter period ended.
    /// The winner is randomly picked using the VRF Coordinator.
    /// https://docs.chain.link/docs/get-a-random-number/v1/
    /// You can think of it that the Chainlik Node calls the
    /// fulfillRandomness function after we call requestRandomness.
    function endLottery() public onlyAfter(endBlock) {
        require(lottery_state == LOTTERY_STATE.OPEN, "Lottery is not running!");
        lottery_state = LOTTERY_STATE.CALCULATING_WINNER;
        bytes32 requestId = requestRandomness(keyhash, fee);
        emit RequestedRandomness(requestId);
    }

    /// @notice Callback function used by VRF Coordinator.
    /// The winner is picked randomly between the players.
    /// Afterwards, the lottery starts over again.
    /// @param _requestId id of the request
    /// @param _randomness the uint256 random number
    function fulfillRandomness(bytes32 _requestId, uint256 _randomness)
        internal
        override
    {
        require(
            lottery_state == LOTTERY_STATE.CALCULATING_WINNER,
            "You aren't there yet!"
        );
        require(_randomness > 0, "random-not-found");
        uint256 indexOfWinner = _randomness % players.length;
        recentWinner = players[indexOfWinner];
        recentWinner.transfer(address(this).balance);

        // Reset
        players = new address payable[](0);
        lottery_state = LOTTERY_STATE.OPEN;
        randomness = _randomness;
        endBlock = block.number + enteringPeriod;
    }

    modifier onlyBefore(uint256 _number) {
        if (block.number >= _number)
            revert("The entering period has already ended.");
        _;
    }
    modifier onlyAfter(uint256 _number) {
        if (block.number <= _number)
            revert("The entering period has not ended yet.");
        _;
    }
}
