from scripts.helpful_scripts import (
    get_accounts,
    fund_with_link,
    get_chainlink_contract,
)
from brownie import web3, chain, reverts


def test_lottery(lottery_contract):
    accounts = get_accounts()

    # ask for the entrance fee
    fee = lottery_contract.entryFee()
    assert fee == web3.toWei(0.1, "ether")
    print("\nAsk for the entrance fee...")
    print(f"The fee is {fee/10**18} ETH")
    print(f"The entering period is {lottery_contract.enteringPeriod()} blocks\n")

    # account 1-9 enters the lottery
    for i in range(1, 10):
        lottery_contract.enter({"from": accounts[i], "value": fee})
        assert lottery_contract.players(i - 1) == accounts[i]
    print("Account 1 to 9 enter the lottery, each buying one ticket\n")

    # Account 1 decides to buy another ticket
    lottery_contract.enter({"from": accounts[1], "value": fee})
    assert lottery_contract.players(9) == accounts[1]
    print("Account 1 buys another ticket\n")

    # Try ending lottery before end of entering period
    with reverts("The entering period has not ended yet."):
        lottery_contract.endLottery({"from": accounts[0]})

    # Fast forward in time to end entering period
    chain.mine(100)
    print("Fast forward 100 blocks in time to end the entering period\n")

    # Try to enter after entering period
    with reverts("The entering period has already ended."):
        lottery_contract.enter({"from": accounts[1], "value": fee})
    print("Account 1 tries to enter after entering period ended")
    print("The transaction is reverted -> The entering period has already ended\n")

    # End lottery and choose winner
    fund_with_link(lottery_contract)
    starting_balance_of_account = accounts[8].balance()
    balance_of_lottery = lottery_contract.balance()

    tx = lottery_contract.endLottery({"from": accounts[0]})
    request_id = tx.events["RequestedRandomness"]["requestId"]
    # mock response from the mock vrf_coordinator
    STATIC_RNG = 777
    get_chainlink_contract("vrf_coordinator").callBackWithRandomness(
        request_id, STATIC_RNG, lottery_contract.address, {"from": accounts[0]}
    )
    assert lottery_contract.recentWinner() == accounts[8]
    assert accounts[8].balance() == starting_balance_of_account + balance_of_lottery
    assert lottery_contract.balance() == 0
    assert lottery_contract.recentWinner() == accounts[8]

    print(
        "Account 0 calls the endLottery function which requests a random number for the VRF Coordinator."
    )
    print(f"The random number returned is {STATIC_RNG}")
    print(
        f"The winner index is picket by {STATIC_RNG} % {10} (#players) = {STATIC_RNG % 10}"
    )
    print(f"Index {STATIC_RNG % 10} belongs to account 8")
    print(
        f"Therefore, account 8 receives the lottery balance of {balance_of_lottery/10**18} ETH"
    )
    print("Afterwards the lottery is reset and starts over again\n")

    # check if entering is possible again
    for i in range(1, 10):
        lottery_contract.enter({"from": accounts[i], "value": fee})
        assert lottery_contract.players(i - 1) == accounts[i]

    # lottery starts over again...
