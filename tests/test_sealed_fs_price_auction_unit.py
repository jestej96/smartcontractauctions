from scripts.helpful_scripts import get_accounts, get_account_index
from brownie import network, exceptions, chain, reverts, web3
import pytest
import json
import logging

def test_first_price(sealed_first_price_auction_contract):
    accounts = get_accounts()
    auction = sealed_first_price_auction_contract

    print("\nTESTING FIRST PRICE\n")
    bids = bidding_period(auction, accounts)

    # start the reveal period
    reveal_period(auction, accounts, bids)

    # reveal period ends
    chain.mine(61)
    print("\n*** Mine 61 blocks to end the reveal period ***")

    # call endAuction
    balance_before = accounts[0].balance()
    auction.endAuction({"from": accounts[3]})
    assert accounts[0].balance() == balance_before + auction.highestBid()
    print("\nAccount 0 calls endAuction")
    print("The beneficiary (account 0) receives the highest bid of  1.6 ETH\n")

    # try endAuction again
    with reverts():
        auction.endAuction({"from": accounts[3]})


def test_second_price(sealed_second_price_auction_contract):
    accounts = get_accounts()
    auction = sealed_second_price_auction_contract

    print("\nTESTING SECOND PRICE\n")
    bids = bidding_period(auction, accounts)
    reveal_period(auction, accounts, bids)

    # reveal period ends
    chain.mine(61)
    print("\n*** Mine 61 blocks to end the reveal period ***\n")

    # call endAuction
    beneficiary_balance_before = accounts[0].balance()
    auction.endAuction({"from": accounts[3]})
    assert (
        accounts[0].balance() == beneficiary_balance_before + auction.secondHighestBid()
    )

    # withdraw difference highestBid and secondHighestBid
    highest_bidder_balance_before = accounts[2].balance()
    tx = auction.withdraw({"from": accounts[2]})
    refund = auction.highestBid() - auction.secondHighestBid()
    assert accounts[2].balance() == highest_bidder_balance_before + refund

    print("Account 0 calls endAuction")
    print("The beneficiary (account 0) receives the second highest bid of 1.5 ETH")
    print("The highest bidder (account 2) receives a refund of 0.1 ETH\n")

    # try endAuction again
    with reverts():
        auction.endAuction({"from": accounts[3]})


def bidding_period(auction, accounts):

    print("*** Bidding period started ***\n")

    hash_secret = lambda string: "0x" + str(web3.toBytes(text=string).hex()).zfill(64)
    hash_bid = lambda bid: web3.solidityKeccak(["uint256", "bool", "bytes32"], bid)

    bids = [
        dict(account_index=1, value=1, fake=False),
        dict(account_index=1, value=2, fake=True),
        dict(account_index=1, value=1.5, fake=False),
        dict(account_index=2, value=0.5, fake=False),
        dict(account_index=2, value=1.6, fake=False),
    ]

    for b in bids:
        b["secret_hashed"] = hash_secret("my_secret")
        b["value"] = web3.toWei(b["value"], "ether")
        b["bid_hashed"] = hash_bid([b["value"], b["fake"], b["secret_hashed"]])

    # make bids from above
    for b in bids:
        tx = auction.bid(
            b["bid_hashed"], {"from": accounts[b["account_index"]], "value": b["value"]}
        )
        print_bid_log(tx)

    # check if the bids are logged correcly for account 1
    logged_bid = auction.bids(accounts[1], 0)
    assert logged_bid[0] == bids[0]["bid_hashed"].hex()
    assert logged_bid[1] == bids[0]["value"]

    logged_bid = auction.bids(accounts[1], 1)
    assert logged_bid[0] == bids[1]["bid_hashed"].hex()
    assert logged_bid[1] == bids[1]["value"]

    return bids


def reveal_period(auction, accounts, bids):

    # start the reveal period
    chain.mine(121)
    print("*** Mine 121 blocks to start the reveal period ***")

    def reveal_parameters(account_index):
        values, fakes, secrets = [], [], []
        for b in bids:
            if b["account_index"] == account_index:
                values += [b["value"]]
                fakes += [b["fake"]]
                secrets += [b["secret_hashed"]]

        return [values, fakes, secrets]

    # reveal bids of account 1
    # this should make account 1 the highest bidder with 1.5 ETH
    # the fake bid of 2 ETH should be refunded
    balance_before = accounts[1].balance()
    tx = auction.reveal(*reveal_parameters(1), {"from": accounts[1]})
    assert auction.highestBidder() == accounts[1]
    assert auction.highestBid() == bids[2]["value"]
    assert accounts[1].balance() == balance_before + bids[1]["value"]

    print("\nAccount 1 reveals its sealed bids:")
    temp = [b for b in bids if b['account_index'] == 1]
    for b in temp:
        b['bid_hashed'] = b['bid_hashed'].hex()
    print(json.dumps(temp, sort_keys=True, indent=2))
    print("After calling reveal, account 1 is the highest bidder with 1.5 ETH")
    print("The fake bid of 2 ETH is automatically refunded.")

    # reveal bids of account 2
    # this should make account 2 the highest bidder with 1.6 ETH
    # the bid of 0.5 ETH should be refunded
    balance_before = accounts[2].balance()
    tx = auction.reveal(*reveal_parameters(2), {"from": accounts[2]})
    assert auction.highestBidder() == accounts[2]
    assert auction.highestBid() == bids[4]["value"]
    assert accounts[2].balance() == balance_before + bids[3]["value"]
    
    print("\nAccount 2 reveals its sealed bids:")
    temp = [b for b in bids if b['account_index'] == 2]
    for b in temp:
        b['bid_hashed'] = b['bid_hashed'].hex()
    print(json.dumps(temp, sort_keys=True, indent=2))
    print("After calling reveal account 2 becomes the highest bidder with 1.6 ETH")
    print("The bid of 0.5 ETH is automatically refunded.")

    # withdraw the overbid funds
    balance_before = accounts[1].balance()
    tx = auction.withdraw({"from": accounts[1]})
    assert accounts[1].balance() == balance_before + bids[0]["value"] + bids[2]["value"]
    print("\nAccount 1 withdraws its overbid funds of 1.5 and 1 ETH.")


def print_bid_log(tx):
    log = tx.events["LogBid"]
    print(f"-> Account {get_account_index(log['bidder'])} commits: {log['cypher']}\n")
