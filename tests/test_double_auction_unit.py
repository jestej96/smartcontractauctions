from scripts.helpful_scripts import (
    get_accounts,
    LOCAL_BLOCKCHAIN_ENVIRONMENTS,
    fund_with_link,
    get_chainlink_contract,
)
from brownie import network, exceptions, chain, reverts, web3, accounts
import pytest
import random


def test_double_auction(double_auction_contract):
    auction = double_auction_contract

    # add 8 more accounts (NOTE: they have a balance of 0)
    print("Add 8 more accounts:")
    for _ in range(8):
        accounts.add()

    hash_secret = lambda string: "0x" + str(web3.toBytes(text=string).hex()).zfill(64)
    hash_bid = lambda bid: web3.solidityKeccak(["uint256", "bytes32"], bid)

    bid_values = [1.05, 0.75, 0.8, 0.65, 0.9, 1, 0.6, 0.95]
    ask_values = [0.85, 0.725, 0.7, 1, 0.45, 0.65, 0.5, 0.8]

    bids = [
        dict(value=web3.toWei(b, "ether"), account_index=i + 2)
        for i, b in enumerate(bid_values)
    ]

    asks = [
        dict(value=web3.toWei(b, "ether"), account_index=i + 10)
        for i, b in enumerate(ask_values)
    ]

    for b in bids + asks:
        b["secret_hashed"] = hash_secret("my_secret")
        b["bid_hashed"] = hash_bid([b["value"], b["secret_hashed"]])

    print("")

    # send bids/asks in bids list
    for b in bids:
        value = b["value"] + web3.toWei(0.5 + random.uniform(0, 1), "ether")
        tx = auction.bid(
            b["bid_hashed"],
            {
                "from": accounts[b["account_index"]],
                "value": value,
            },
        )
        print(
            f"Buyer {b['account_index']} commits {b['bid_hashed'].hex()} and sends {value/10**18:.3f} ETH"
        )

    print("")
    for b in asks:
        tx = auction.ask(
            b["bid_hashed"],
            {"from": accounts[b["account_index"]]},
        )
        print(f"Seller {b['account_index']} commits {b['bid_hashed'].hex()} ")

    # begin reveal period
    chain.mine(101)
    print("\nFast forward 100 blocks to start reveal period\n")

    # reveal bid/asks in bids list
    for b in bids:
        tx = auction.revealBid(
            b["value"],
            b["secret_hashed"],
            {"from": accounts[b["account_index"]]},
        )
        print(f"Buyer {b['account_index']} reveals bid of {b['value']/10**18:.3f} ETH")

    print("")

    for b in asks:
        tx = auction.revealAsk(
            b["value"],
            b["secret_hashed"],
            {"from": accounts[b["account_index"]]},
        )
        print(f"Seller {b['account_index']} reveals ask of {b['value']/10**18:.3f} ETH")

    # end reveal period
    chain.mine(101)
    print("\nFast forward 100 blocks to end reveal period\n")

    MECHANISM = "vcg"
    print(f"Clearance using {MECHANISM.upper()}")

    mechanisms = {"vcg": 0, "trade_reduction": 1, "mcafee": 2}

    sorted_bids = auction.clearance(
        mechanisms[MECHANISM], {"from": accounts[0]}
    ).return_value
    print("Buyer prices descending:", [b[1] / 10**18 for b in sorted_bids[0]])
    print("Seller prices ascending:", [b[1] / 10**18 for b in sorted_bids[1]])
    print("Break even index:", sorted_bids[2])
    print("Buffer:", auction.buffer() / 10**18, "ETH")
    print("Pay:", sorted_bids[3] / 10**18)
    print("Get:", sorted_bids[4] / 10**18)

    print("Buyer balances: ", [accounts[i].balance() / 10**18 for i in range(2, 10)])
    print("Seller balances:", [accounts[i].balance() / 10**18 for i in range(10, 18)])
    print("Notice: seller accounts were newly created with balance of 0.")
    print("Auctioneer balance: ", accounts[0].balance() / 10**18, "(Started with 90 ETH)")
    print("Contract balance: ", auction.balance() / 10**18, "(Started with 10 ETH)")
