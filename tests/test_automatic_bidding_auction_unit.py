from scripts.helpful_scripts import get_accounts
from brownie import network, exceptions, chain, reverts, web3
import pytest


def test_get_bid_increment(automatic_bidding_auction_contract):
    auction = automatic_bidding_auction_contract
    ETH2USD = 1000
    priceSteps = [0, 1, 5, 25, 100, 250, 500, 1000, 2500, 5000]  # no decimals
    bidIncrements = [5, 25, 50, 100, 250, 500, 1000, 2500, 5000, 10000]  # two decimals

    for price, inc in zip(priceSteps, bidIncrements):
        assert auction.getBidIncrement(
            web3.toWei(price / ETH2USD, "ether")
        ) / 10**18 == inc / (ETH2USD * 100)


def test_bidding(automatic_bidding_auction_contract):

    auction = automatic_bidding_auction_contract
    accounts = get_accounts()

    # accunt 1 bids 5 ETH
    print("")
    tx = auction.bid({"from": accounts[1], "value": web3.toWei(5, "ether")})
    print_bid_log(tx)
    assert auction.highestBidder() == accounts[1]
    assert auction.highestBindingBid() == 0.00005 * 10**18

    # account 2 bids 3 ETH -> not enough to overbid
    # account 1 remains highest bidder
    # highest binding bid increases to 3 ETH + increment
    tx = auction.bid({"from": accounts[2], "value": web3.toWei(3, "ether")})
    print_bid_log(tx)
    assert auction.highestBidder() == accounts[1]
    assert auction.highestBindingBid() == 3.00005 * 10**18

    # account 1 bids 5 ETH (raises its funds to 10 ETH)
    # account 1 remains highest bidder
    # highest binding bid is not increases
    print("Account 1 raises its maximum bid to 10 ETH")
    tx = auction.bid({"from": accounts[1], "value": web3.toWei(5, "ether")})
    print_bid_log(tx)
    assert auction.highestBidder() == accounts[1]
    assert auction.highestBindingBid() == 3.00005 * 10**18

    # account 2 bids 3 ETH again (sum of funds = 6 ETH) -> not enough to overbid
    # account 1 remains highest bidder
    # highest binding bid increases to 6 ETH + increment
    tx = auction.bid({"from": accounts[2], "value": web3.toWei(3, "ether")})
    print_bid_log(tx)
    assert auction.highestBidder() == accounts[1]
    assert auction.highestBindingBid() == 6.05 * 10**18

    # account 2 withdraws its funds (sum of funds = 6 ETH)
    tx = auction.withdraw({"from": accounts[2]})
    print_withdrawal_log(tx)

    # account 3 bids 8 ETH again -> not enough to overbid
    # account 1 remains highest bidder
    # highest binding bid increases to 8 ETH + increment
    tx = auction.bid({"from": accounts[3], "value": web3.toWei(8, "ether")})
    print_bid_log(tx)
    assert auction.highestBidder() == accounts[1]
    assert auction.highestBindingBid() == 8.1 * 10**18

    # account 4 bids 11 ETH again -> enough to overbid
    # account 4 becomes highest bidder
    # highest binding bid is prev highest binding + increment
    tx = auction.bid({"from": accounts[4], "value": web3.toWei(11, "ether")})
    print_bid_log(tx)
    assert auction.highestBidder() == accounts[4]
    assert auction.highestBindingBid() == 10.1 * 10**18

    # account 5 bids 11.001 ETH again -> enough to overbid
    # because the highest (max) bid of account 4 is 11 ETH
    # here the increment isn't applied
    # account 5 becomes highest bidder
    # highest binding bid is 11.001
    tx = auction.bid({"from": accounts[5], "value": web3.toWei(11.001, "ether")})
    print_bid_log(tx)
    assert auction.highestBidder() == accounts[5]
    assert auction.highestBindingBid() == 11.001 * 10**18

    # account 6 tries to overbid by just 0.001 ETH
    # this doesn't work because overbidding the current price
    # of at least the bid increment is necessary
    with reverts("Bid must be greater then highest binding bid + bid increment"):
        print("-> Account 6 bids 11.002 ETH")
        print("Transaction is reverted!")
        print("The bid must be greater then highest binding bid + bid increment")
        print("")
        tx = auction.bid({"from": accounts[6], "value": web3.toWei(11.002, "ether")})

    # end bidding period
    chain.mine(101)
    print("\n*** END BIDDING PERIOD ***\n")

    # try bidding after end
    with reverts("The auction has already ended."):
        print("-> Account 6 bids 12 ETH")
        print("Transaction is reverted!")
        print("The auction has already ended.\n")
        tx = auction.bid({"from": accounts[6], "value": web3.toWei(12, "ether")})

    # beneficiary claims highest bid
    print("\nThe beneficiary withdraws the highest binding bid.")
    balance_before = accounts[0].balance()
    tx = auction.withdraw({"from": accounts[0]})
    print_withdrawal_log(tx)
    assert accounts[0].balance() == balance_before + auction.highestBindingBid()

    # beneficiary tries to withdraw again
    print("\nThe beneficiary tries to withdraw again.")
    with reverts("Beneficiary already withdrawed."):
        print("Transaction is reverted!")
        print("Beneficiary already withdrawed.\n")
        tx = auction.withdraw({"from": accounts[0]})

    # account 1 withdraws its overbid funds of 10 ETH
    print("\nAccount 1 withdraws its overbid funds of 10 ETH")
    funds = auction.fundsByBidder(accounts[1])
    balance_before = accounts[1].balance()
    tx = auction.withdraw({"from": accounts[1]})
    print_withdrawal_log(tx)
    assert accounts[1].balance() == balance_before + funds

    # account 1 tries to withdraw again
    print("\nAccount 1 tries to withdraw again.")
    with reverts("There are no funds to withdraw."):
        print("Transaction is reverted!")
        print("There are no funds to withdraw.\n")
        tx = auction.withdraw({"from": accounts[1]})


def print_bid_log(tx):

    get_account_index = lambda address: list(get_accounts()).index(address)
    log = tx.events["LogBid"]
    print(
        f"-> Account {get_account_index(log['bidder'])} bids {log['bid'] / 10**18:.3f} ETH"
    )
    print(f"Highest bidder account index: {get_account_index(log['highestBidder'])}")
    print(f"Highest bid: {log['highestBid'] / 10**18:.3f} ETH")
    print(f"Highest binding bid: {log['highestBindingBid'] / 10**18:.3f} ETH")
    print(f"Bid increment: {log['bidIncrement'] / 10**18:.5f} ETH")
    print("")


def print_withdrawal_log(tx):
    get_account_index = lambda address: list(get_accounts()).index(address)
    log = tx.events["LogWithdrawal"]
    print(
        f"-> Account {get_account_index(log['withdrawalAccount'])} withdraws {log['amount'] / 10**18:.3f} ETH"
    )
    print("")
