from scripts.helpful_scripts import get_accounts, LOCAL_BLOCKCHAIN_ENVIRONMENTS
from brownie import network, exceptions, chain, reverts, web3
import pytest


def test_buying_after_50(dutch_auction_contract):
    auction = dutch_auction_contract
    accounts = get_accounts()

    # check the starting price
    print("\nCheck auction parameters:")
    print(f"Starting price: {auction.startingPrice()/10**18} ETH")
    print(f"Discount period: {auction.discountPeriod()}")
    print(f"Discount value: {auction.discountValue()/10**18} ETH")
    print(f"Auctions ends at block: {auction.endBlock()}")
    print(f"Current block number: {web3.eth.block_number}\n")

    # wait for discount and check if price is correct
    chain.mine(20)
    assert auction.getPrice() == web3.toWei(0.8, "ether")
    print(f"We wait for 20 blocks to pass and check the price:")
    print(f"The current discounted price is {auction.getPrice()/10**18} ETH\n")

    # buy from account 1 with value less then price
    balance_before = accounts[1].balance()
    with reverts("Not enough funds"):
        tx = auction.buy({"from": accounts[1], "value": web3.toWei(0.7, "ether")})
    assert balance_before == accounts[1].balance()
    print("Account 1 tries to buy, but sends not enough ETH")
    print("The buy transaction is reverted -> Not enough funds\n")

    # wait another 30 blocks
    chain.mine(30)
    assert auction.getPrice() == web3.toWei(0.5, "ether")
    print(f"We wait for another 30 blocks to pass and check the price again:")
    print(f"The current discounted price is {auction.getPrice()/10**18} ETH\n")

    # buy from account 1 with value higher then price,
    # check refund and payout
    beneficiary_balance_before = accounts[0].balance()
    buyer_balance_before = accounts[1].balance()
    tx = auction.buy({"from": accounts[1], "value": web3.toWei(0.8, "ether")})
    assert accounts[0].balance() == beneficiary_balance_before + web3.toWei(
        0.5, "ether"
    )
    assert accounts[1].balance() == buyer_balance_before - web3.toWei(0.5, "ether")
    print("Account 1 now calls the buy function and transfers 0.6 ETH")
    print("The beneficiary receives the current price -> 0.5 ETH")
    print("Account 1 receives a refund of 0.1 ETH")
    print("The auction ends.\n")

    # try buying after selling
    with reverts("Someone else was quicker"):
        tx = auction.buy({"from": accounts[2], "value": web3.toWei(0.9, "ether")})
    print("After the auction ended, Account 2 tries to call the buy function.")
    print("The buy transaction is reverted -> Someone else was quicker\n")


def test_buying_after_expired(dutch_auction_contract):
    accounts = get_accounts()
    chain.mine(101)
    with reverts("Auction expired"):
        dutch_auction_contract.buy(
            {"from": accounts[1], "value": web3.toWei(0.9, "ether")}
        )
