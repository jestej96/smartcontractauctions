from scripts.helpful_scripts import get_accounts, get_chainlink_contract, fund_with_link
from brownie import Lottery, network, config, web3, chain
import time


def test_american_auction(american_auction_contract):

    accounts = get_accounts()
    auction = american_auction_contract

    fund_with_link(auction, amount=10 * 10**18)

    print("\nRead auction parameters...")
    print(f"Starting block: {auction.startBlock()}")
    print(f"Bidding blocks: {auction.biddingBlocks()}")
    print(f"Minimum bid: {auction.minBid()/10**18} ETH")
    print(f"Winning probability at start: {auction.startProba()/10**18}")

    # account 1 bids
    tx = auction.bid({"from": accounts[1], "value": web3.toWei(0.1, "ether")})
    request_id = tx.events["RequestedRandomness"]["requestId"]
    # mock response from the mock vrf_coordinator
    STATIC_RNG = 1 * 10**17
    get_chainlink_contract("vrf_coordinator").callBackWithRandomness(
        request_id, STATIC_RNG, auction.address, {"from": accounts[0]}
    )
    info(auction, STATIC_RNG, 1)

    chain.mine(10)
    print("\nWait for 10 blocks to pass")

    # account 2 bids
    tx = auction.bid({"from": accounts[1], "value": web3.toWei(0.1, "ether")})
    request_id = tx.events["RequestedRandomness"]["requestId"]
    # mock response from the mock vrf_coordinator
    STATIC_RNG = 1 * 10**17
    get_chainlink_contract("vrf_coordinator").callBackWithRandomness(
        request_id, STATIC_RNG, auction.address, {"from": accounts[0]}
    )
    info(auction, STATIC_RNG, 2)

    print("\nWait for 33 blocks to pass")
    chain.mine(33)

    # account 3 bids
    tx = auction.bid({"from": accounts[3], "value": web3.toWei(0.1, "ether")})
    request_id = tx.events["RequestedRandomness"]["requestId"]
    # mock response from the mock vrf_coordinator
    STATIC_RNG = 5 * 10**17
    get_chainlink_contract("vrf_coordinator").callBackWithRandomness(
        request_id, STATIC_RNG, auction.address, {"from": accounts[0]}
    )
    info(auction, STATIC_RNG, 3)

    print("\nWait for 40 blocks to pass")
    chain.mine(40)

    # account 4 bids
    tx = auction.bid({"from": accounts[4], "value": web3.toWei(0.1, "ether")})
    request_id = tx.events["RequestedRandomness"]["requestId"]
    # mock response from the mock vrf_coordinator
    STATIC_RNG = 4 * 10**17
    get_chainlink_contract("vrf_coordinator").callBackWithRandomness(
        request_id, STATIC_RNG, auction.address, {"from": accounts[0]}
    )
    info(auction, STATIC_RNG, 4)
    print("\nAuction ended, the winner is account 4")

    assert auction.auctionEnded()
    assert auction.latestBidder() == accounts[4]

    # auctioneer withdraws all funds
    funds_collected = auction.getFunds({"from": accounts[0]}) / 10**18
    print(f"Funds collected: {funds_collected} ETH")
    balance_before = accounts[0].balance()
    auction.withdrawFunds({"from": accounts[0]})
    assert accounts[0].balance() == balance_before + funds_collected * 10**18

    print(f"Auctioneer withdrawed {funds_collected} ETH")


def info(auction, random_number, account):
    print("")
    print(f"Bid from account {account}")
    print("Read current state...")
    blocks_passed = web3.eth.block_number - auction.startBlock()
    print("Blocks passed:", blocks_passed)
    p = ((0.5 - 0.0001) / 100) * blocks_passed + 0.0001
    print(f"Winning probabiliy: ~{p}")
    print(f"Predetermined random number: {(random_number % 10**18):.4e}")
    print(f"Test if random number in: [0, {p * 10**18}]")
    if p * 10**18 >= random_number % 10**18:
        print("Inside intervall => pick winner")
    else:
        print("Outside intervall => bad luck")
