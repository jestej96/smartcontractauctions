from scripts.helpful_scripts import (
    get_accounts,
    LOCAL_BLOCKCHAIN_ENVIRONMENTS,
)
from scripts.deploy_dutch_auction import deploy_dutch_auction
from brownie import network, web3, chain, DutchAuction, reverts, exceptions
import pytest
import time


def test_dutch_auction_integration():
    if network.show_active() in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip()

    accounts = get_accounts()

    # rinkeby average block time is 15 seconds
    auction = deploy_dutch_auction(
        0, web3.toWei(0.01, "ether"), web3.toWei(0.001, "ether"), 1, 20
    )

    print("\nCheck auction parameters:")
    print(f"Starting price: {auction.startingPrice()/10**18} ETH")
    print(f"Discount value: {auction.discountValue()/10**18} ETH")
    print(f"Discount period: {auction.discountPeriod()}")
    print(f"Auctions ends at block: {auction.endBlock()}")
    print(f"Current block number: {web3.eth.block_number}\n")

    print(f"We wait for 90 seconds and check the current price every 5 seconds")
    for i in range(16):
        print(f"\nThe current price is {auction.getPrice() /10**18} ETH")
        time.sleep(5)

    # NOTE
    # Debugging functionality relies on the debug_traceTransaction RPC method.
    # If you are using Infura this endpoint is unavailable.
    # Attempts to access this functionality will raise an RPCRequestError.

    # print("\nNow account 1 calls buy transaction with sufficient funds:")
    # tx = auction.buy({"from": accounts[1], "value": auction.getPrice()})
    # tx.wait(1)
    # time.sleep(1)

    # print("Buy transaction is successful and the beneficiary receives the current price.")
    # print(f"Check if auction is not marked as closed: {auction.sold()}")
