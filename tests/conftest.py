from scripts.helpful_scripts import (
    LOCAL_BLOCKCHAIN_ENVIRONMENTS,
)
from brownie import network
from scripts.deploy_open_fs_price_auction import deploy_open_fs_price_auction
from scripts.deploy_automatic_bidding_auction import deploy_automatic_bidding_auction
from scripts.deploy_american_auction import deploy_american_auction
from scripts.deploy_dutch_auction import deploy_dutch_auction
from scripts.deploy_sealed_fs_price_auction import deploy_sealed_fs_price_auction
from scripts.deploy_double_auction import deploy_double_auction
from scripts.deploy_lottery import deploy_lottery
import pytest


@pytest.fixture()
def check_local_blockchain_envs():
    if network.show_active() not in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip()


@pytest.fixture()
def check_online_blockchain_envs():
    if network.show_active() in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        pytest.skip()


@pytest.fixture()
def lottery_contract():
    return deploy_lottery()


@pytest.fixture()
def open_first_price_auction_contract():
    return deploy_open_fs_price_auction()


@pytest.fixture()
def open_second_price_auction_contract():
    return deploy_open_fs_price_auction(second_price=True)


@pytest.fixture()
def sealed_first_price_auction_contract():
    return deploy_sealed_fs_price_auction()


@pytest.fixture()
def sealed_second_price_auction_contract():
    return deploy_sealed_fs_price_auction(second_price=True)


@pytest.fixture()
def automatic_bidding_auction_contract():
    return deploy_automatic_bidding_auction()


@pytest.fixture()
def dutch_auction_contract():
    return deploy_dutch_auction()


@pytest.fixture()
def double_auction_contract():
    return deploy_double_auction()


@pytest.fixture()
def american_auction_contract():
    return deploy_american_auction()
