from scripts.helpful_scripts import get_accounts, get_account_index
from brownie import network, exceptions, chain, reverts, web3
import pytest


def test_first_price(open_first_price_auction_contract):

    accounts = get_accounts()
    auction = open_first_price_auction_contract

    print("\nTESTING FIRST PRICE\n")
    bidding_period(auction, accounts)

    # end auction
    chain.sleep(121)
    chain.mine(1)
    print("\n*** Fast forward time to end the bidding period. ***")

    # call end auction and check if highest bid is payed
    balance_before = accounts[0].balance()
    highest_bid = auction.fundsByBidder(auction.highestBidder())
    tx = auction.auctionEnd({"from": accounts[1]})
    assert accounts[0].balance() == balance_before + highest_bid
    print("\nAccount 1 calls endAuction.")
    print(f"The beneficiary receives {highest_bid/10**18} ETH.")

    # try ending again after end
    with reverts("The function auctionEnd has already been called."):
        tx = auction.auctionEnd({"from": accounts[1]})
    print("\nAccount 1 calls endAuction again.")
    print(f"Reverts -> The function auctionEnd has already been called.")

    # try bidding afer auction end
    with reverts("The auction has already ended."):
        tx = auction.bid({"from": accounts[5], "value": web3.toWei(1, "ether")})
    print("\nAccount 5 tries bidding after auction end again.")
    print(f"Reverts -> The auction has already ended.")
    print("")


def test_second_price(open_second_price_auction_contract):

    accounts = get_accounts()
    auction = open_second_price_auction_contract

    print("\nTESTING SECOND PRICE\n")
    bidding_period(auction, accounts)

    # end auction
    chain.sleep(121)
    chain.mine(1)
    print("\n*** Fast forward time to end the bidding period. ***")

    beneficiary_balance_before = accounts[0].balance()
    highest_bidder_balance_before = accounts[1].balance()

    highest_bid = auction.fundsByBidder(auction.highestBidder())
    second_highest_bid = auction.fundsByBidder(auction.secondHighestBidder())

    # check if only the second price is payed
    tx = auction.auctionEnd({"from": accounts[1]})
    assert accounts[0].balance() == beneficiary_balance_before + second_highest_bid
    print("\nAccount 1 calls endAuction.")
    print(f"The beneficiary receives {second_highest_bid/10**18} ETH.")

    # check if difference of highest bid and second highest bid can be withdrawn
    auction.withdraw({"from": accounts[1]})
    refund = highest_bid - second_highest_bid
    assert accounts[1].balance() == highest_bidder_balance_before + refund
    print("\nAccount 1 withdraws the difference of highest and second highest bid.")
    print(f"Account 1 receives {refund/10**18} ETH.")


def bidding_period(auction, accounts):

    # make a valid bid
    tx = auction.bid({"from": accounts[1], "value": web3.toWei(0.25, "ether")})
    assert auction.highestBidder() == accounts[1]
    assert auction.fundsByBidder(auction.highestBidder()) == web3.toWei(0.25, "ether")
    print_bid_log(tx, auction)

    # try to bid less then the highest bid
    # alternative: with pytest.raises(exceptions.VirtualMachineError):
    with reverts("There is already a higher or equal bid."):
        tx = auction.bid({"from": accounts[2], "value": web3.toWei(0.02, "ether")})
    assert auction.highestBidder() == accounts[1]
    assert auction.fundsByBidder(auction.highestBidder()) == web3.toWei(0.25, "ether")
    print_bid_log(tx, auction)

    # overbid
    tx = auction.bid({"from": accounts[2], "value": web3.toWei(0.5, "ether")})
    assert auction.highestBidder() == accounts[2]
    assert auction.fundsByBidder(auction.highestBidder()) == web3.toWei(0.5, "ether")
    print_bid_log(tx, auction)

    # overbid
    tx = auction.bid({"from": accounts[3], "value": web3.toWei(1, "ether")})
    assert auction.highestBidder() == accounts[3]
    assert auction.fundsByBidder(auction.highestBidder()) == web3.toWei(1, "ether")
    print_bid_log(tx, auction)

    # overbid again by bidding only the difference of funds and highestBid
    tx = auction.bid({"from": accounts[1], "value": web3.toWei(0.9, "ether")})
    assert auction.highestBidder() == accounts[1]
    assert auction.fundsByBidder(auction.highestBidder()) == web3.toWei(1.15, "ether")
    print_bid_log(tx, auction)

    # withdraw overbid funds
    balance_before = accounts[2].balance()
    tx = auction.withdraw({"from": accounts[2]})
    assert tx.return_value
    assert accounts[2].balance() == balance_before + web3.toWei(0.5, "ether")
    print("Account 2 withdraws its overbid 0.5 ETH")

    # try withdrawing again
    balance_before = accounts[2].balance()
    tx = auction.withdraw({"from": accounts[2]})
    assert tx.return_value
    assert balance_before == accounts[2].balance()
    print("\nAccount 2 tries to withdraw again.")
    print("Its balance doesn't change since there are no funds to withdraw left.")

    # try withdrawing from highest bidder account
    with reverts("Highest bidder can't withdraw during bidding period."):
        tx = auction.withdraw({"from": accounts[1]})
    print("\nAccount 1 (highest bidder) tries to withdraw.")
    print("Reverts -> Highest bidder can't withdraw during bidding period.")

    # try auctionEnd() before end
    with reverts("The auction has not ended yet."):
        tx = auction.auctionEnd({"from": accounts[1]})
    print("\nAccount 1 (highest bidder) tries to end the running auction.")
    print("Reverts -> The auction has not ended yet.")


def print_bid_log(tx, auction):
    log = tx.events["HighestBidIncreased"]
    print(
        f"-> Account {get_account_index(log['bidder'])} bids {log['bid'] / 10**18:.3f} ETH"
    )
    print(f"Highest bidder: Account {get_account_index(auction.highestBidder())}")
    print(f"Highest bid: {log['highestBid'] / 10**18:.3f} ETH\n")
