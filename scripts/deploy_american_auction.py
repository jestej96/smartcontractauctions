from brownie import AmericanAuction, network, config, web3

from scripts.helpful_scripts import get_accounts, get_chainlink_contract


def deploy_american_auction(
    account_index=0,
    bidding_blocks=100,
    minimum_bid=web3.toWei(0.1, "ether"),
    starting_probability=0.001 * 10**18,
):  
    accounts = get_accounts()
    auction = AmericanAuction.deploy(
        bidding_blocks,
        minimum_bid,
        starting_probability,
        get_chainlink_contract("vrf_coordinator").address,
        get_chainlink_contract("link_token").address,
        config["networks"][network.show_active()]["fee"],
        config["networks"][network.show_active()]["keyhash"],
        {"from": accounts[account_index]},
        publish_source=config["networks"][network.show_active()].get("verify"),
    )
    print(f"Auction deployed to {auction.address}")
    return auction


def main():
    deploy_american_auction()
