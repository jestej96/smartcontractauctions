from brownie import AutomaticBiddingAuction, network, config, web3

from scripts.helpful_scripts import get_accounts, get_chainlink_contract


def deploy_automatic_bidding_auction(
    account_index=0,
    bidding_blocks=100,
):
    accounts = get_accounts()
    auction = AutomaticBiddingAuction.deploy(
        bidding_blocks,
        get_chainlink_contract("eth_usd_price_feed").address,
        {"from": accounts[account_index]},
        publish_source=config["networks"][network.show_active()].get("verify"),
    )
    print(f"Auction deployed to {auction.address}")
    return auction


def main():
    deploy_automatic_bidding_auction()
