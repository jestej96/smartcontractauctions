from brownie import (
    accounts,
    network,
    config,
    MockV3Aggregator,
    VRFCoordinatorMock,
    LinkToken,
    Contract,
    interface,
)

FORKED_LOCAL_ENVIRONMENTS = ["mainnet-fork"]
LOCAL_BLOCKCHAIN_ENVIRONMENTS = ["development", "ganache-local"]


def get_accounts():
    """
    Returns an accounts list depending on the given parameters and network.
    """

    if (
        network.show_active() in LOCAL_BLOCKCHAIN_ENVIRONMENTS
        or network.show_active() in FORKED_LOCAL_ENVIRONMENTS
    ):
        return accounts

    metamask_accounts = []
    for name in ["account_1", "account_2", "account_3"]:
        if accounts.add(config["wallets"][name]) != "":
            metamask_accounts.append(accounts.add(config["wallets"][name]))

    if len(metamask_accounts) == 0:
        raise Exception("Account details not specified in config!")

    return metamask_accounts


def get_chainlink_contract(contract_name):
    """This function will grab the contract addresses from the brownie config
    if defined, otherwise, it will deploy a mock version of that contract, and
    return that mock contract.

        Args:
            contract_name (string)

        Returns:
            brownie.network.contract.ProjectContract: The most recently deployed
            version of this contract.
    """

    contract_to_mock = {
        "eth_usd_price_feed": MockV3Aggregator,
        "vrf_coordinator": VRFCoordinatorMock,
        "link_token": LinkToken,
    }

    contract_type = contract_to_mock[contract_name]
    if network.show_active() in LOCAL_BLOCKCHAIN_ENVIRONMENTS:
        if len(contract_type) <= 0:
            deploy_mocks()
        contract = contract_type[-1]
    else:
        contract_address = config["networks"][network.show_active()][contract_name]
        contract = Contract.from_abi(
            contract_type._name, contract_address, contract_type.abi
        )
    return contract


DECIMALS = 8  # decimals of the MockV3Aggregator
INITIAL_VALUE = 100000000000  # initial USD / ETH value


def deploy_mocks(decimals=DECIMALS, initial_value=INITIAL_VALUE):
    accounts = get_accounts()
    MockV3Aggregator.deploy(decimals, initial_value, {"from": accounts[0]})
    link_token = LinkToken.deploy({"from": accounts[0]})
    VRFCoordinatorMock.deploy(link_token.address, {"from": accounts[0]})
    print("Deployed mock contracts!")


def fund_with_link(
    contract_address, account=None, link_token=None, amount=0.1 * 10**18
):  # 0.1 LINK
    account = account if account else get_accounts()[0]
    link_token = link_token if link_token else get_chainlink_contract("link_token")
    tx = link_token.transfer(contract_address, amount, {"from": account})
    # link_token_contract = interface.LinkTokenInterface(link_token.address)
    # tx = link_token_contract.transfer(contract_address, amount, {"from": account})
    tx.wait(1)
    print("Funded contract with LINK.")
    return tx


def get_account_index(address):
    return list(get_accounts()).index(address)
