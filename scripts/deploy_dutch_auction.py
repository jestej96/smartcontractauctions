from brownie import DutchAuction, network, config, web3

from scripts.helpful_scripts import (
    get_accounts,
)


def deploy_dutch_auction(
    account_index=0,
    starting_price=web3.toWei(1, "ether"),
    discount_value=web3.toWei(0.1, "ether"),
    discount_period=10,
    bidding_blocks=100,
):
    accounts = get_accounts()
    auction = DutchAuction.deploy(
        starting_price,
        discount_value,
        discount_period,
        bidding_blocks,
        {"from": accounts[account_index]},
        publish_source=config["networks"][network.show_active()].get("verify"),
    )
    print(f"Auction deployed to {auction.address}")
    return auction


def main():
    deploy_dutch_auction()
