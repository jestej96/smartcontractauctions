from brownie import OpenFSPriceAuction, network, config

from scripts.helpful_scripts import (
    get_accounts,
)


def deploy_open_fs_price_auction(
    account_index=0, starting_price=0, bidding_time=120, second_price=False
):
    accounts = get_accounts()
    auction = OpenFSPriceAuction.deploy(
        starting_price,
        bidding_time,
        second_price,
        {"from": accounts[account_index]},
        publish_source=config["networks"][network.show_active()].get("verify"),
    )
    print(f"Auction deployed to {auction.address}")
    return auction


def main():
    deploy_open_fs_price_auction()
