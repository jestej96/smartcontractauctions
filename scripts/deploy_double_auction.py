from brownie import DoubleAuction, network, config, web3

from scripts.helpful_scripts import get_accounts


def deploy_double_auction(
    account_index=0,
    bidding_blocks=100,
    reveal_blocks=100,
    auctioneer_deposit=web3.toWei(10, "ether"),
):
    accounts = get_accounts()
    auction = DoubleAuction.deploy(
        bidding_blocks,
        reveal_blocks,
        {"from": accounts[account_index], "value": auctioneer_deposit},
        publish_source=config["networks"][network.show_active()].get("verify"),
    )
    print(f"Auction deployed to {auction.address}")
    return auction


def main():
    deploy_double_auction()
