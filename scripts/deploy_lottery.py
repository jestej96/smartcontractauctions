from scripts.helpful_scripts import get_accounts, get_chainlink_contract
from brownie import Lottery, network, config, web3


def deploy_lottery(
    account_index=0,
    entry_fee=web3.toWei(0.1, "ether"),
    entering_blocks=100,
):
    account = get_accounts()[account_index]
    lottery = Lottery.deploy(
        entry_fee,
        entering_blocks,
        get_chainlink_contract("vrf_coordinator").address,
        get_chainlink_contract("link_token").address,
        config["networks"][network.show_active()]["fee"],
        config["networks"][network.show_active()]["keyhash"],
        {"from": account},
        publish_source=config["networks"][network.show_active()].get("verify", False),
    )
    print("Deployed lottery!")
    return lottery


def main():
    deploy_lottery()
