from brownie import SealedFSPriceAuction, network, config

from scripts.helpful_scripts import (
    get_accounts,
)


def deploy_sealed_fs_price_auction(
    account_index=0, bidding_blocks=120, reveal_blocks=60, second_price=False
):
    accounts = get_accounts()
    auction = SealedFSPriceAuction.deploy(
        bidding_blocks,
        reveal_blocks,
        second_price,
        {"from": accounts[account_index]},
        publish_source=config["networks"][network.show_active()].get("verify"),
    )
    print(f"Auction deployed to {auction.address}")
    return auction


def main():
    deploy_sealed_fs_price_auction()
